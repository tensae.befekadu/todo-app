function iscompleted() {
    // Get the checkbox
    var checkBox = document.getElementById("defaultCheck1");
    // Get the output text
    var text = document.getElementById("itemsid");

    // If the checkbox is checked, display the output text
    if (checkBox.checked == true) {
        text.style.textDecoration = "line-through";
    } else {
        text.style.display = "none";
    }
}