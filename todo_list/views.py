from django.shortcuts import render, redirect
from urllib import request
from .models import List
from .forms import ListForm
from django.contrib import messages
from django.http import HttpResponseRedirect


def home(request):

    if request.method == 'POST':
        form = ListForm(request.POST or None)

        if form.is_valid():
            form.save()
            items = List.objects.all
            messages.success(request, ('Item is added to List'))
            return render(request, 'home.html', {'items': items})

    else:
        items = List.objects.all
        return render(request, 'home.html', {'items': items})


def about(request):
    my_name = "Tensae Befekadu"
    return render(request, 'about.html', {'name': my_name})


def delete(request, list_id):
    item = List.objects.get(pk=list_id)
    item.delete()
    messages.success(request, ('Item is removed from List'))
    return redirect('home')


def edit(requesr, list_id):
    pass
